<?php

namespace Nitra\TextBlockBundle\Controller\TextBlock;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nitra\StoreBundle\Lib\Globals;

class TextBlockController extends Controller
{
    protected $textBlockRepository          = 'NitraTextBlockBundle:TextBlock';
    
    /** @return \Doctrine\ODM\MongoDB\DocumentManager */
    protected function getDocumentManager() { return $this->get('doctrine.odm.mongodb.document_manager'); }
    
    /**
     * Текстовый блок
     * @Route("/text_block/{location}", name="text_block")
     */
    public function textBlockAction($location, $template = 'NitraTextBlockBundle:TextBlock:TextBlock.html.twig')
    {
        //получение текущего магазина 
        $store = Globals::getStore();
        
        $textBlocks = $this->getDocumentManager()->getRepository($this->textBlockRepository)->findBy(array(
            'location.$id'  => $location,
            'isActive'      => true,
            'stores.$id'    => new \MongoId($store['id']),
        ));
        return $this->render($template, array(
            'text_blocks'   => $textBlocks,
        ));
    }
}